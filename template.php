<?php
// $Id$

/**
 * Implementation of hook_theme().
 */
function seed_theme(&$existing, $type, $theme, $path) {
  include_once './' . drupal_get_path('theme', 'seed') . '/theme-settings.php';

  // Since we are rebuilding the theme registry and the theme settings' default
  // values may have changed, make sure they are saved in the database properly.
  seed_theme_get_default_settings($theme);

  // Custom theme items
  
  $items['fieldset'] = array(
    'template' => 'object',
    'path' => drupal_get_path('theme', 'seed') .'/templates',
    'arguments' => array('element' => array()),
  );

  $items['grid_block'] = array(
   'arguments' => array('element' => NULL, 'name' => NULL, 'grid' => 0, 'push' => 0, 'clear' => FALSE),
  );
  
  $items['region'] = array(
   'arguments' => array('block' => NULL, 'name' => NULL),
  );

  return $items;
}


/******************************************************************************
 * Preprocess functions                                                       *
 ******************************************************************************/

/**
 * General preprocess
 */
function seed_preprocess(&$vars, $hook) {
  // The theme hook is being called
  $vars['hook'] = $hook;
}

/**
 * Page preprocess
 */
function seed_preprocess_page(&$vars) {
  // Get the settings for the current theme
  global $theme_key;
  $settings = theme_get_settings($theme_key);
  $seed_path = drupal_get_path('theme','seed');  
  $vars['settings'] = $settings;

  // For mobile browsers
  drupal_set_html_head('<meta name="viewport" content="width = device-width, height = 600, user-scalable = no" />');
  $vars['head']   = drupal_get_html_head();
  $vars['styles'] .= '<link rel="stylesheet" type="text/css" media="only screen and (max-width: 480px), only screen and (max-device-width: 480px)" href="/' . $seed_path . '/css/mobile.css" />'; 
  // The above method of altering the css requires subthemes to alter the styles variable directly
  // Using drupal_add_css doesn't really work, because it breaks the modifications made by the decruft basetheme
  // We'll just have to wait for hook_css_alter in Drupal 7

  // Body classes //
  $body_classes   = explode(' ', $vars['body_classes']);                   // Existing
  $body_classes[] = theme_get_setting('seed_sidebar_layout');              // Sidebar layout

  if (user_access('administer blocks', $user)) {
    $body_classes[] = $settings['seed_showgrid']  ? 'showgrid'  : '';      // Show Grid
    $body_classes[] = $settings['seed_wireframe'] ? 'wireframe' : '';      // Wireframe
  }

  // Module specific body classes:
  if (module_exists('panels') && function_exists('panels_get_current_page_display')) {
    $body_classes[] = (panels_get_current_page_display()) ? 'panels' : ''; // Panels page
  }
  // @TODO: og, spaces, ubercart ...

  // Create class list separated by spaces
  $body_classes = array_filter($body_classes);
  $vars['body_classes'] = implode(' ', $body_classes);

  // Add a unique page id
  $vars['body_id'] = 'path-' . strtolower(preg_replace('/[^a-zA-Z0-9-]+/', '-', drupal_get_path_alias($_GET['q'])));
    
  // Sitename with link
  if ($vars['site_name']) {
    $vars['l_site_name'] = l(
      $vars['site_name'],
      '<front>',
      array('attributes' => array('rel'   => 'home', 'title' => t('Home')))
    );
  }
  
  // Footer Message and Mission //
  // These are depricated in Drupal 7, but we'll still grudgingly support them here
  $vars['highlight'] = theme('grid_block', $vars['mission'], 'mission') . $vars['highlight'];
  $vars['footer']   .= theme('grid_block', $vars['footer_message'], 'footer-message');

  // Re cast content as a block
  $vars['content']   = theme('grid_block', $vars['content'], 'content');

  // Main Menu //
  // Make dropdown if enabled
  // @TODO: Make a theme function for this?
  $vars['main_menu'] = '';
  if ($vars['primary_links']) {
    if ($settings['seed_primary_menu_dropdown']) {
      $vars['main_menu'] = menu_tree(variable_get('menu_primary_links_source', 'primary-links'));
      $vars['main_menu'] = preg_replace('/<ul class="menu/', '<ul class="links menu dropdown', $vars['main_menu'], 1);
    }
    else {
      $vars['main_menu'] = theme('links', $vars['primary_links'], array('class' => 'links menu'));
    }
  }

  // Secondary Menu //
  $vars['secondary_menu'] = '';
  if ($vars['secondary_links']) {
    $vars['secondary_menu'] = theme('links', $vars['secondary_links'], array('class' => 'links menu'));
  }

  // Tabs //
  // Consolidate tabs as links and split the primary and secondary into two variables
  if ($tabs = menu_primary_local_tasks()) {
    $vars['tabs']  = "<ul id=\"primary-tabs\" class=\"links menu tabs\">\n". $tabs ."</ul>\n";
  }
  if ($tabs2 = menu_secondary_local_tasks()) {
    $vars['tabs2'] = "<ul id=\"secondary-tabs\" class=\"links menu tabs\">\n". $tabs2 ."</ul>\n";
  }


  // Sidebar Layout Classes //
  $cols = array(
    'main'   => NULL, // Auto
    'first'  => $vars['left']  ? $settings['seed_sidebar_first_width']  : 0,
    'second' => $vars['right'] ? $settings['seed_sidebar_second_width'] : 0,
  );

  $arrangement = array(
    'sidebars-split'      => $order = array('first', 'main'  , 'second'),
    'sidebars-both-left'  => $order = array('first', 'second', 'main'  ),
    'sidebars-both-right' => $order = array('main' , 'first' , 'second'),
  );

  $cols = seed_grid_row_calc(24, $cols, $arrangement[$settings['seed_sidebar_layout']]);

  $vars['sidebar_first_grid_classes']  = $cols['first']['class'];
  $vars['sidebar_second_grid_classes'] = $cols['second']['class'];
  $vars['main_grid_classes']           = $cols['main']['class'];
}

/**
 * Node preprocess
 */
function seed_preprocess_node(&$vars) {
  // Node Classes
  $classes = array(
    $vars['hook'],                                       // 'node'
    'node-' . $vars['node']->type,                       // Node Type
    !$vars['node']->status ? 'unpublished'  : '',        // Published?
    $vars['teaser']        ? 'teaser'       : '',        // Teaser or full-node
    $vars['sticky']        ? 'sticky'       : '',        // Sticky?
    $vars['promote']       ? 'promoted'     : '',        // Promoted?
    $vars['zebra'],                                      // Odd or even
    $vars['skinr'],                                      // Add Skinr classes if present
    ($vars['uid'] == $vars['user']->uid) ? 'mine' : '',  // By current user ?
    'author-' . strtolower(preg_replace('/[^a-zA-Z0-9-]+/', '-', $vars['node']->name)), // Author.
  );
  $classes = array_filter($classes);                     // Remove empty elements

  // Attributes
  $vars['attr']['id']    = 'node-' . $vars['node']->nid;
  $vars['attr']['class'] = implode(' ', $classes);       // Implode class list with spaces
  
  // template_preprocess_node() uses class = "links inline"
  // redo it without the 'inline' class
  $vars['links'] = $vars['links'] ? theme('links', $vars['node']->links, array('class' => 'links')) : '';
  $vars['terms'] = theme('links', $vars['taxonomy'], array('class' => 'links'));

  // Add node_top and node_bottom region content
  // Note: These regions still get themed in template_preprocess_page()
  // even though they don't get used. This could be a performance drain.
  if (!$vars['teaser']) {
    $vars['node_top']    = theme('blocks', 'node_top');
    $vars['node_bottom'] = theme('blocks', 'node_bottom');
  }

  // Ubercart
  if (module_exists('uc_product') && uc_product_is_product($vars['type'])) {
     $attr['class'] = 'product ' . $attr['class'];          // product class
     array_unshift($vars['template_files'], 'product');   // product.tpl.php
  }
}

/**
 * Comment preprocess
 */
function seed_preprocess_comment(&$vars) {
  global $user;

  // Comment Classes
  $classes = array(
    $vars['hook'],                                                             // 'comment'
    $vars['zebra'],                                                            // Odd or even
    ($vars['comment']->new) ? 'new' : '',                                      // New?
    ($vars['comment']->status == COMMENT_NOT_PUBLISHED) ? 'unpublished' : '',  // Published?
    ($vars['comment']->uid == $vars['node']->uid)       ? 'by-author'   : '',  // By node author
    ($vars['comment']->uid == $user->uid && $user->uid) ? 'mine'        : '',  // By current user?
    ($vars['comment']->uid == 0)                        ? 'by-anon'     : '',  // Anonymous user?
  );
  $classes  = array_filter($classes);                                          // Remove empty elements

  // Attributes
  $vars['attr']['id']    = 'comment-' . $vars['comment']->cid;
  $vars['attr']['class'] = implode(' ', $classes);                             // Implode class list with spaces
}

/**
 * Block preprocess
 */
function seed_preprocess_block(&$vars) {
  // Block classes
  $classes = array(
    $vars['hook'],                       // 'block'
    'block-' . $vars['block']->module,   // Block's module
    $vars['zebra'],                      // Block is odd or even
  );
  $classes = array_filter($classes);     // Remove empty elements

  // Attributes
  $vars['attr']['id']    = "block-{$vars['block']->module}-{$vars['block']->delta}";
  $vars['attr']['class'] = implode(' ', $classes);  // Implode class list with spaces

  // Title and content
  $vars['title']   = !empty($vars['block']->subject) ? $vars['block']->subject : '';
  $vars['content'] = $vars['block']->content;
}

/**
 * Fieldset preprocess
 */
function seed_preprocess_fieldset(&$vars) {
  $element = $vars['element'];

  $attr = isset($element['#attributes']) ? $element['#attributes'] : array();
  $attr['class']  = !empty($attr['class']) ? $attr['class'] : '';
  $attr['class'] .= ' fieldset';
  $attr['class'] .= !empty($element['#collapsible']) ? ' collapsible' : '';
  $attr['class'] .= !empty($element['#collapsible']) && !empty($element['#collapsed']) ? ' collapsed' : '';
  $vars['attr']   = $attr;

  $description = !empty($element['#description']) ? "<div class='description'>{$element['#description']}</div>" : '';
  $children    = !empty($element['#children']) ? $element['#children'] : '';
  $value       = !empty($element['#value']) ? $element['#value'] : '';
  $vars['content'] = $description . $children . $value;
  $vars['title']   = !empty($element['#title']) ? $element['#title'] : '';

  $vars['hook'] = 'fieldset';
}

/******************************************************************************
 * Theme functions                                                            *
 ******************************************************************************/

/**
 * Theme a grid block
 */
function seed_grid_block($element, $name, $grid = 0, $push = 0, $clear = FALSE) {

  $grid_classes .= $clear ? ' clear' : '';

  if ($grid > 0) {
    $grid_classes .= ' column grid-' . $grid;
    $grid_classes .= ( $push > 0 ) ? ( ' push-' .  $push ): '';
    $grid_classes .= ( $push < 0 ) ? ( ' pull-' . -$push ): '';
  }

  $output = '';
  if ($element) {
    $output .= '<div id="block-' . $name . '" class="block grid-block' . $grid_classes . '">' . "\n";
    $output .= '<div id="block-' . $name . '-inner" class="inner block-inner">' . "\n";
    $output .= $element;
    $output .= '</div>' . "\n";
    $output .= '</div>' . "\n";
  }
  return $output;
}

/**
 * Theme a region
 */
function seed_region($blocks, $name) {
  $output = '';
  if ($blocks) {
    $output .= '<div id="region-' . $name . '" class="region">' . "\n";
    $output .= $blocks . "\n";
    $output .= '</div>' . "\n";
  }
  return $output;
}


/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return
 *   A string containing the breadcrumb output.
 */
function seed_breadcrumb($breadcrumb) {
  // Options
  $show_crumb = theme_get_setting('seed_breadcrumb');
  // Determine if we are to display the breadcrumb.
  if ( $show_crumb == 'yes' || ($show_crumb == 'admin' && arg(0) == 'admin') ) {

    $show_home      = theme_get_setting('seed_breadcrumb_home');
    $show_title     = theme_get_setting('seed_breadcrumb_title');
    $show_trailing  = theme_get_setting('seed_breadcrumb_trailing');
    $separator      = theme_get_setting('seed_breadcrumb_separator');

    // Optionally get rid of the homepage link.
    if (!$show_home) {
      array_shift($breadcrumb);
    }

    // Return the breadcrumb with separators.
    if (!empty($breadcrumb)) {
      $trailing_separator = $title = '';
      if ($show_title) {
        $trailing_separator = $separator;
        $title = menu_get_active_title();
      }
      elseif ($show_trailing) {
        $trailing_separator = $separator;
      }
      return '<div class="breadcrumb">' . implode($separator, $breadcrumb) . "$trailing_separator$title</div>";
    }
  }
  // Otherwise, return an empty string.
  return '';
}

/**
 * Implementation of theme_help
 */
function seed_help() {
  if ($help = menu_get_active_help()) {
    // Cast help as a faux block
    return theme('grid_block',$help,'help');;
  }
}

/**
 * Implementation of theme_node_submitted
 */
function seed_node_submitted($node) {
  return t('Posted by !username on @datetime',
    array(
      '!username' => theme('username', $node),
      '@datetime' => format_date($node->created),
    )
  );
}

/**
 * Implementation of theme_comment_submitted
 */
function seed_comment_submitted($comment) {
  return t('By !username on @datetime.',
    array(
      '!username' => theme('username', $comment),
      '@datetime' => format_date($comment->timestamp)
    )
  );
}

/**
 * Implementation of theme_username
 * Hides or shows username '(not verified)' text
 */
function seed_username($object) {
  if ($object->uid && $object->name) {
    // Shorten the name when it is too long or it will break many tables.
    if (drupal_strlen($object->name) > 20) $name = drupal_substr($object->name, 0, 15) .'...';
    else $name = $object->name;

    if (user_access('access user profiles')) {
      $output = l(
        $name,
        'user/'. $object->uid,
        array('attributes' => array('title' => t('View user profile.')))
      );
    }
    else $output = check_plain($name);
  }
  else if ($object->name) {
    // Sometimes modules display content composed by people who are
    // not registered members of the site (e.g. mailing list or news
    // aggregator modules). This clause enables modules to display
    // the true author of the content.
    if (!empty($object->homepage)) {
      $output = l(
        $object->name,
        $object->homepage,
        array('attributes' => array('rel' => 'nofollow'))
      );
    }
    else $output = check_plain($object->name);

    // Only display not verified text if this setting is enabled.
    if (theme_get_setting('seed_user_notverified_display'))
      $output .= ' ('. t('not verified') .')';
  }

  else $output = check_plain(variable_get('anonymous', t('Anonymous')));

  return $output;
}

/**
 * Implementation of theme_form_element
 */
function seed_form_element($element, $value) {
  $output = '';

  // This is also used in the installer, pre-database setup.
  $t = get_t();

  // Add a wrapper id
  $attr = array('class' => '');
  $attr['id'] = !empty($element['#id']) ? "{$element['#id']}-wrapper" : NULL;

  // Type logic
  $label_attr = array();
  $label_attr['for'] = !empty($element['#id']) ? $element['#id'] : '';

  if (!empty($element['#type']) && in_array($element['#type'], array('checkbox', 'radio'))) {
    $label_type = 'label';
    $attr['class'] .= ' form-item form-option';
  }
  else {
    $label_type = 'label';
    $attr['class'] .= ' form-item';
  }

  // Generate required markup
  $required_title = $t('This field is required.');
  $required = !empty($element['#required']) ? "<span class='form-required' title='{$required_title}'>*</span>" : '';

  // Generate label markup
  if (!empty($element['#title'])) {
    $title = $t('!title: !required', array('!title' => filter_xss_admin($element['#title']), '!required' => $required));
    $label_attr = drupal_attributes($label_attr);
    $output .= "<{$label_type} {$label_attr}>{$title}</{$label_type}>";
    $attr['class'] .= ' form-item-labeled';
  }

  // Add child values
  $output .= "$value";

  // Description markup
  $output .= !empty($element['#description']) ? "<div class='description'>{$element['#description']}</div>" : '';

  // Render the whole thing
  $attr = drupal_attributes($attr);
  $output = "<div {$attr}>{$output}</div>";

  return $output;
}

/**
 * Implementation of theme_textfield
 */
function seed_textfield($element) {
  if ($element['#size'] >= 30) {
    $element['#size'] = '';
    $element['#attributes']['class'] = isset($element['#attributes']['class']) ? "{$element['#attributes']['class']} fluid" : "fluid";
  }
  return theme_textfield($element);
}

/**
 * Implementation of theme_password
 */
function seed_password($element) {
  if ($element['#size'] >= 30 || $element['#maxlength'] >= 30) {
    $element['#size'] = '';
    $element['#attributes']['class'] = isset($element['#attributes']['class']) ? "{$element['#attributes']['class']} fluid" : "fluid";
  }
  return theme_password($element);
}

/**
 * Implementation of theme_file
 * Reduces the size of upload fields which are by default far too long.
 */
function seed_file($element) {
  _form_set_class($element, array('form-file'));
  $attr = $element['#attributes'] ? ' '. drupal_attributes($element['#attributes']) : '';
  return theme('form_element', $element, "<input type='file' name='{$element['#name']}' id='{$element['#id']}' size='15' {$attr} />");
}

/******************************************************************************
 * Helper Functions                                                           *
 ******************************************************************************/

/**
 * Grid row calculations
 * @param $width
 *   The total width in grid units.
 * @param $cols
 *   An keyed array listing the width of each column in grid units.
 *   NULL means auto. Unclaimed width will be divided over NULL values.
 * @param $order
 *   Optional: An array listing the keys of $cols in the order they are to be displayed.
 *   $cols is assumed to list the columns in HTML source order.
 *   From this, the push and pull classes are calculated to move
 *   the columns into the specified order.
 * @return
 *   The $cols array with each column expanded to hold the folowing properties:
 *   grid          => width in grid units
 *   source_order  => source order of the columns (starts at 0)
 *   source_pos    => position of the left edge in grid units before displacement
 *   display_order => display order of the columns (starts at 0)
 *   display_pos   => display position of the left edge in grid units after displacement
 *   class         => grid classes for the column
 */
function seed_grid_row_calc($width, $cols, $order = NULL) {
  // Divide unclaimed grid units evenly amongst the NULL values
  $nulls = 0;
  foreach ($cols as &$col)  if ($col === NULL)  $nulls++ ;           // count null values

  $unclaimed  = $width - array_sum($cols);
  $auto_width = (int)($unclaimed / $nulls);
  foreach ($cols as &$col)  if ($col === NULL)  $col = $auto_width ; // replace NULL with auto_width

  // If there's a remainder, find the best way to use it
  $remainder = $width - array_sum($cols);
  if ($remainder) {
    // Find the key of the largest column.
    // This is probably the best place to dump a remainder
    $key = array_search(max($cols),$cols);
    $cols[$key] += $remainder;
  }

  // Cycle through source columns, build a more complete model.
  // Determine the source order and position before any push or pull actions
  $source_pos = 0;
  $source_order = 0;
  foreach ($cols as $name => $grid) {
    $cols[$name] = array(
      'grid'         => $grid,
      'source_order' => $source_order,
      'source_pos'   => $source_pos,
    );
    $source_order++;
    $source_pos += $grid;
  }

  // If a different display order is specified,
  // Cycle through and calculate the display position and coresponding displacement for each column.
  if ($order) {
    $display_pos = 0;
    $display_order = 0;
    foreach ($order as $name) {
      $cols[$name]['display_order'] = $display_order;
      $cols[$name]['display_pos']   = $display_pos;
      $cols[$name]['displace']      = $display_pos - $cols[$name]['source_pos']; // +push / -pull in grid units.
      $display_order++;
      $display_pos += $cols[$name]['grid'];
    }
  }

  // Generate classes
  foreach ($cols as &$col) {
    if ($col['grid'] > 0) {
      $col['class']  = 'column grid-' . $col['grid'];
      $col['class'] .= ($col['displace'] > 0) ? ' push-' .  $col['displace'] : '';
      $col['class'] .= ($col['displace'] < 0) ? ' pull-' . -$col['displace'] : '';
    }
    else $col['class'] = '';
  }

  return $cols;
}

/******************************************************************************
 * Development Scraps and Notes                                               *
 ******************************************************************************/

 /**
 * Search result preprocessing
 */
//function fusion_core_preprocess_search_result(&$vars) {
//  static $search_zebra = 'even';
//
//  $search_zebra = ($search_zebra == 'even') ? 'odd' : 'even';
//  $vars['search_zebra'] = $search_zebra;
//  $result = $vars['result'];
//  $vars['url'] = check_url($result['link']);
//  $vars['title'] = check_plain($result['title']);
//
//  // Check for snippet existence. User search does not include snippets.
//  $vars['snippet'] = '';
//  if (isset($result['snippet']) && theme_get_setting('search_snippet')) {
//    $vars['snippet'] = $result['snippet'];
//  }
//
//  $info = array();
//  if (!empty($result['type']) && theme_get_setting('search_info_type')) {
//    $info['type'] = check_plain($result['type']);
//  }
//  if (!empty($result['user']) && theme_get_setting('search_info_user')) {
//    $info['user'] = $result['user'];
//  }
//  if (!empty($result['date']) && theme_get_setting('search_info_date')) {
//    $info['date'] = format_date($result['date'], 'small');
//  }
//  if (isset($result['extra']) && is_array($result['extra'])) {
//    // $info = array_merge($info, $result['extra']);  Drupal bug?  [extra] array not keyed with 'comment' & 'upload'
//    if (!empty($result['extra'][0]) && theme_get_setting('search_info_comment')) {
//      $info['comment'] = $result['extra'][0];
//    }
//    if (!empty($result['extra'][1]) && theme_get_setting('search_info_upload')) {
//      $info['upload'] = $result['extra'][1];
//    }
//  }
//
//  // Provide separated and grouped meta information.
//  $vars['info_split'] = $info;
//  $vars['info'] = implode(' - ', $info);
//
//  // Provide alternate search result template.
//  $vars['template_files'][] = 'search-result-'. $vars['type'];
//}


// From Fusion Preprocess Block
//
//  global $theme_info, $user;
//  static $regions, $sidebar_first_width, $sidebar_last_width, $grid_name, $grid_width, $grid_fixed;
//
//  // Initialize block region grid info once per page
//  if (!isset($regions)) {
//    $grid_name = substr(theme_get_setting('theme_grid'), 0, 7);
//    $grid_width = (int)substr($grid_name, 4, 2);
//    $grid_fixed = (substr(theme_get_setting('theme_grid'), 7) != 'fluid') ? 1 : 0;
//    $sidebar_first_width = (block_list('sidebar_first')) ? theme_get_setting('sidebar_first_width') : 0;
//    $sidebar_last_width = (block_list('sidebar_last')) ? theme_get_setting('sidebar_last_width') : 0;
//    $regions = fusion_core_set_regions($grid_width, $sidebar_first_width, $sidebar_last_width);
//  }
//
//  // Increment block count for current block's region, add first/last position class
//  $regions[$vars['block']->region]['count'] ++;
//  $region_count = $regions[$vars['block']->region]['count'];
//  $total_blocks = $regions[$vars['block']->region]['total'];
//  $vars['position'] = ($region_count == 1) ? 'first' : '';
//  $vars['position'] .= ($region_count == $total_blocks) ? ' last' : '';
//
//  // Set a default block width if not already set by Skinr
//  if (!isset($vars['skinr']) || (strpos($vars['skinr'], $grid_name) === false)) {
//    // Stack blocks vertically in sidebars by setting to full sidebar width
//    if ($vars['block']->region == 'sidebar_first') {
//      $width = $sidebar_first_width;
//    }
//    elseif ($vars['block']->region == 'sidebar_last') {
//      $width = $sidebar_last_width;
//    }
//    else {
//      // Default block width = region width divided by total blocks, adding any extra width to last block
//      $region_width = ($grid_fixed) ? $regions[$vars['block']->region]['width'] : $grid_width;  // fluid grid regions = 100%
//      $width_adjust = (($region_count == $total_blocks) && ($region_width % $total_blocks)) ? $region_width % $total_blocks : 0;
//      $width = ($total_blocks) ? floor($region_width / $total_blocks) + $width_adjust : 0;
//    }
//    $vars['skinr'] = (isset($vars['skinr'])) ? $vars['skinr'] . ' ' . $grid_name . $width : $grid_name . $width;
//  }
//
//  if (isset($vars['skinr']) && (strpos($vars['skinr'], 'superfish') !== false) &&
//     ($vars['block']->module == 'menu' || ($vars['block']->module == 'user' && $vars['block']->delta == 1))) {
//    $superfish = ' sf-menu';
//    $superfish .= (strpos($vars['skinr'], 'superfish-vertical')) ? ' sf-vertical' : '';
//    $vars['block']->content = preg_replace('/<ul class="menu/i', '<ul class="menu' . $superfish, $vars['block']->content, 1);
//  }
//
  // Add block edit links for admins
//  if (user_access('administer blocks', $user) /* && theme_get_setting('block_config_link') */) {
//    $vars['edit_links'] = '<div class="seed-edit">'. implode(' ', seed_edit_links($vars['block'])) .'</div>';
//  }



// From Fusion Preprocess_node
//
  // Render Ubercart fields into separate variables for node-product.tpl.php
  // Pulled from Fusion Core
  // @TODO: Test and modify this to needs
//  if (module_exists('uc_product') && uc_product_is_product($vars['type'])) {
//    $classes[] = 'product';        // Node is a product
//    $node = node_build_content(node_load($vars['nid']));
//    $vars['seed_uc_image']         = drupal_render($node->content['image']);
//    $vars['seed_uc_body']          = drupal_render($node->content['body']);
//    $vars['seed_uc_display_price'] = drupal_render($node->content['display_price']);
//    $vars['seed_uc_add_to_cart']   = drupal_render($node->content['add_to_cart']);
//    $vars['seed_uc_sell_price']    = drupal_render($node->content['sell_price']);
//    $vars['seed_uc_cost']          = drupal_render($node->content['cost']);
//    $vars['seed_uc_weight'] = (!empty($node->weight)) ? drupal_render($node->content['weight']) : '';     // Hide weight if empty
//    if ($vars['seed_uc_weight'] == '') {
//      unset($node->content['weight']);
//    }
//    $dimensions = !empty($node->height) && !empty($node->width) && !empty($node->length);                 // Hide dimensions if empty
//    $vars['seed_uc_dimensions'] = ($dimensions) ? drupal_render($node->content['dimensions']) : '';
//    if ($vars['seed_uc_dimensions'] == '') {
//      unset($node->content['dimensions']);
//    }
//    $list_price = !empty($node->list_price) && $node->list_price > 0;                                     // Hide list price if empty or zero
//    $vars['fusion_uc_list_price'] = ($list_price) ? drupal_render($node->content['list_price']) : '';
//    if ($vars['seed_uc_list_price'] == '') {
//      unset($node->content['list_price']);
//    }
//    $vars['seed_uc_additional'] = drupal_render($node->content);                                          // Render remaining fields
//  }