<?php
// $Id$

/**
 * Return the theme settings' default values from the .info and save them into the database.
 *
 * @param $theme
 *   The name of theme.
 */
function seed_theme_get_default_settings($theme) {
  $themes = list_themes();

  // Get the default values from the .info file.
  $defaults = !empty($themes[$theme]->info['settings']) ? $themes[$theme]->info['settings'] : array();

  if (!empty($defaults)) {
    // Get the theme settings saved in the database.
    $settings = theme_get_settings($theme);
    // Don't save the toggle_node_info_ variables.
    if (module_exists('node')) {
      foreach (node_get_types() as $type => $name) {
        unset($settings['toggle_node_info_' . $type]);
      }
    }
    // Save default theme settings.
    variable_set(
      str_replace('/', '_', 'theme_' . $theme . '_settings'),
      array_merge($defaults, $settings)
    );
    // If the active theme has been loaded, force refresh of Drupal internals.
    if (!empty($GLOBALS['theme_key'])) {
      theme_get_setting('', TRUE);
    }
  }
  // Return the default settings.
  return $defaults;
}

/**
 * Implementation of THEMEHOOK_settings() function.
 *
 * @param $saved_settings
 *   An array of saved settings for this theme.
 * @param $subtheme_defaults
 *   Allow a subtheme to override the default values.
 * @return
 *   A form array.
 */
function seed_settings($saved_settings, $subtheme_defaults = array()) {

  // Get the default values from the .info file.
  $defaults = seed_theme_get_default_settings('seed');

  // Allow a subtheme to override the default values.
  $defaults = array_merge($defaults, $subtheme_defaults);

  // Merge the saved variables and their default values.
  $settings = array_merge($defaults, $saved_settings);

  $form['seed'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Seed theme settings'),
    '#collapsible'   => TRUE,
  );

  $form['seed']['layout'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Layout'),
    '#description'   => t('This theme allows for up to two sidebars. These settings define how the sidebars will be positioned if used.'),
    '#collapsible'   => TRUE,
    '#collapsed'     => TRUE,

  );
  // Sidebar layout
  $form['seed']['layout']['seed_sidebar_layout'] = array(
    '#type'          => 'radios',
    '#title'         => t('Select a sidebar layout for your theme'),
    '#default_value' => $settings['seed_sidebar_layout'],
    '#options'       => array(
      'sidebars-split' => t('Split sidebars'),
      'sidebars-both-left' => t('Both sidebars left'),
      'sidebars-both-right' => t('Both sidebars right'),
    ),
  );
  $form['seed']['layout']['seed_sidebar_first_width'] = array(
    '#type'          => 'textfield',
    '#title'         => t('First sidebar width'),
    '#description'   => t('The width in grid units'),
    '#default_value' => $settings['seed_sidebar_first_width'],
    '#size'          => 2,
    '#maxlength'     => 2,
  );
  $form['seed']['layout']['seed_sidebar_second_width'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Second sidebar width'),
    '#description'   => t('The width in grid units'),
    '#default_value' => $settings['seed_sidebar_second_width'],
    '#size'          => 2,
    '#maxlength'     => 2,
  );

  $form['seed']['general'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('General'),
    '#collapsible'   => TRUE,
    '#collapsed'     => TRUE,
  );

  $form['seed']['general']['seed_primary_menu_dropdown'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Main menu as dropdown'),
    '#default_value' => $settings['seed_primary_menu_dropdown'],
  );

  $form['seed']['general']['seed_user_notverified_display'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display "not verified" for unregistered usernames'),
    '#default_value' => $settings['seed_user_notverified_display'],
  );

  $form['seed']['breadcrumb'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Breadcrumb'),
    '#collapsible'   => TRUE,
    '#collapsed'     => TRUE,
  );
  $form['seed']['breadcrumb']['seed_breadcrumb'] = array(
    '#type'          => 'select',
    '#title'         => t('Display breadcrumb'),
    '#default_value' => $settings['seed_breadcrumb'],
    '#options'       => array(
                          'yes'   => t('Yes'),
                          'admin' => t('Only in admin section'),
                          'no'    => t('No'),
                        ),
  );
  $form['seed']['breadcrumb']['seed_breadcrumb_separator'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Breadcrumb separator'),
    '#description'   => t('Text only. Don’t forget to include spaces.'),
    '#default_value' => $settings['seed_breadcrumb_separator'],
    '#size'          => 5,
    '#maxlength'     => 10,
  );
  $form['seed']['breadcrumb']['seed_breadcrumb_home'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Show home page link in breadcrumb'),
    '#default_value' => $settings['seed_breadcrumb_home'],
  );
  $form['seed']['breadcrumb']['seed_breadcrumb_trailing'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Append a separator to the end of the breadcrumb'),
    '#default_value' => $settings['seed_breadcrumb_trailing'],
    '#description'   => t('Useful when the breadcrumb is placed just before the title.'),
  );
  $form['seed']['breadcrumb']['seed_breadcrumb_title'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Append the content title to the end of the breadcrumb'),
    '#default_value' => $settings['seed_breadcrumb_title'],
    '#description'   => t('Useful when the breadcrumb is not placed just before the title.'),
  );
  $form['seed']['themedev'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Theme development'),
    '#collapsible'   => TRUE,
    '#collapsed'     => TRUE,
  );
  $form['seed']['themedev']['seed_showgrid'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Show grid for theme administrators'),
    '#default_value' => $settings['seed_showgrid'],
    '#description'   => t('Only shows the grid for users with "administer themes" permission'),
  );
  $form['seed']['themedev']['seed_wireframe'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Show wireframe for theme administrators'),
    '#default_value' => $settings['seed_wireframe'],
    '#description'   => t('Only shows the wireframe for users with "administer themes" permission'),
  );

  return $form;
}
