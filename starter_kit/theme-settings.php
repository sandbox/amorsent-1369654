<?php

// Include the definition of seed_settings() and seed_theme_get_default_settings().
include_once './' . drupal_get_path('theme', 'seed') . '/theme-settings.php';

/**
 * Implementation of THEMEHOOK_settings() function.
 *
 * @param $saved_settings
 *   An array of saved settings for this theme.
 * @return
 *   A form array.
 */
function STARTER_KIT_form_system_theme_settings_alter(&$form, &$form_state) {
  // Replace STARTER_KIT with the name of your theme
  // Return the form
  return $form;
}
