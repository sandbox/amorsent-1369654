<?php
// $Id$

/**
 * @file page.tpl.php
 *
 * Theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $css: An array of CSS files for the current page.
 * - $directory: The directory the theme is located in, e.g. themes/garland or
 *   themes/garland/minelli.
 * - $is_front: TRUE if the current page is the front page. Used to toggle the mission statement.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Page metadata:
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $head_title: A modified version of the page title, for use in the TITLE tag.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $body_classes: A set of CSS classes for the BODY tag. This contains flags
 *   indicating the current layout (multiple columns, single column), the current
 *   path, whether the user is logged in, and so on.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $mission: The text of the site mission, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $search_box: HTML to display the search box, empty if search has been disabled.
 * - $primary_links (array): An array containing primary navigation links for the
 *   site, if they have been configured.
 * - $secondary_links (array): An array containing secondary navigation links for
 *   the site, if they have been configured.
 *
 * Page content
 *
 * - $breadcrumb: The breadcrumb trail for the current page.
 * - $title: The page title, for use in the actual HTML content.
 * - $help: Dynamic help text, mostly for admin pages.
 * - $messages: HTML for status and error messages. Should be displayed prominently.
 * - $tabs: Tabs linking to any sub-pages beneath the current page (e.g., the view
 *   and edit tabs when displaying a node).
 * - $feed_icons: A string of all feed icons for the current page.
 * - $footer_message: The footer message as defined in the admin settings.
 * - $closure: Final closing markup from any modules that have altered the page.
 *   This variable should always be output last, after all other dynamic content.
*
 * Regions
 * - $header : The header region.
 * - $content: The main content of the current Drupal page.
 * - $left: The HTML for the left sidebar.
 * - $right: The HTML for the right sidebar.
 * - $footer : The footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see seed_preprocess_page()
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

  <head>
    <title><?php print $head_title; ?></title>
    <?php print $head; ?>
    <?php print $styles; ?>
    <?php print $scripts; ?>
  </head>

  <body id="<?php print $body_id; ?>" class="<?php print $body_classes; ?>">

      <div id="accessibility-links">
        <a href="#main-content-area"><?php print t('Skip to Main Content'); ?></a>
      </div>

    <?php if ($messages): ?>
      <div id="growl">
        <?php print $messages; ?>
      </div>
    <?php endif; ?>

    <div id="body-wrapper">

      <?php if ($logo || $site_name || $site_slogan || $branding): ?>
        <div id="masthead"><div id="masthead-limiter" class="limiter">

            <?php
              // Construct and theme the Branding grid_block
              if ($logo){
                $branding = l(
                  "<img src=\"$logo\" alt=\"logo\"/>",
                  '<front>' ,
                  array('attributes'=>array('rel'=>'home', 'title' => t('Home'), 'id'=>'logo'), 'html'=>TRUE)
                );
              };
              if($site_name){
                $site_name_tag = $title ? 'div' : 'h1';
                $branding .= "<$site_name_tag id=\"site-name\">$l_site_name</$site_name_tag>";
                $branding .= "<div id=\"site-slogan\">$site_slogan</div>";
              }

              print theme('grid_block', $branding, 'branding');
            ?>

            <?php print theme('grid_block', $main_menu, 'main-menu', 0, 0, TRUE); ?>
            <?php print theme('grid_block', $secondary_menu, 'secondary-menu'); ?>
        </div></div>
      <?php endif; ?>

      <?php if ($header): ?>
        <div id="header"><div class="limiter">
          <?php print theme('region', $header, 'header'); ?>
        </div></div>
      <?php endif; ?>

      <div id="page"><div id="page-limiter" class="limiter">
        <div id="main" class="<?php print $main_grid_classes; ?>">

          <?php print theme('region',$highlight, 'highlight'); ?>

          <?php
            $breadcrumb_title = $breadcrumb;
            $breadcrumb_title .= '<a name="main-content-area" id="main-content-area"></a>';
            $breadcrumb_title .= $title ? ('<h1 class="page-title">' . $title . '</h1>') : '';

            print theme('grid_block', $breadcrumb_title , 'breadcrumb-title');
          ?>

          <?php print theme('grid_block',$tabs . $tabs2,'tabs'); ?>
          <?php print theme('region', $help, 'help'); ?>

          <?php print theme('region', $content_top, 'content-top'); ?>
          <?php print theme('region', $content, 'content'); ?>
          <?php print theme('region', $content_bottom, 'content-bottom'); ?>

          <?php print theme('grid_block', $feed_icons, 'feed-icons'); ?>
        </div>

        <?php if ($left): ?>
          <div id="left" class="<?php print $sidebar_first_grid_classes; ?>">
            <?php print theme('region', $left, 'sidebar-first'); ?>
          </div>
        <?php endif; ?>

        <?php if ($right): ?>
          <div id="right" class="<?php print $sidebar_second_grid_classes; ?>">
            <?php print theme('region', $right, 'sidebar-second'); ?>
          </div>
        <?php endif; ?>

      </div></div>

      <?php if ($footer): ?>
        <div id="footer"><div id="footer-limiter" class="limiter">
          <?php print theme('region', $footer, 'footer'); ?>
        </div></div>
      <?php endif; ?>
    </div>

    <?php print $closure ?>

  </body>
</html>
