<div <?php if ($attr) print drupal_attributes($attr) ?>>
  <div class="inner <?php print $hook; ?>-inner">

    <?php if ($edit_links): ?>
      <?php print $edit_links; ?>
    <?php endif; ?>

    <?php if ($title): ?>
      <h2><?php print $title; ?></h2>
    <?php endif;?>

    <?php if ($content): ?>
      <div class="content">
        <?php print $content ?>
      </div>
    <?php endif;?>
    
  </div>
</div>