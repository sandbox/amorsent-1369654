<div <?php if ($attr) print drupal_attributes($attr) ?>>
  <div class="inner <?php print $hook; ?>-inner">

    <?php print $picture ?>

    <?php if (!$page): ?>
      <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
    <?php endif; ?>

    <?php if ($submitted): ?>
      <div class="meta">
        <span class="submitted"><?php print $submitted ?></span>
      </div>
    <?php endif; ?>

    <?php print theme('region', $node_top, 'node-top'); // only available on full view ?>

    <?php if ($content): ?>
      <div class="content">
        <?php print $content ?>
      </div>
    <?php endif;?>

    <?php if ($terms): ?>
      <div class="terms">
        <?php print $terms; ?>
      </div>
    <?php endif;?>

    <?php if ($links): ?>
       <div class="links">
         <?php print $links; ?>
       </div>
    <?php endif; ?>
  </div>

  <?php print theme('region', $node_bottom, 'node-bottom'); // only available on full view ?>

</div>
