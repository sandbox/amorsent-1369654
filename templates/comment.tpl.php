<div <?php if($attr) print drupal_attributes($attr) ?>>
  <div class="inner <?php print $hook; ?>-inner">

    <?php print $picture ?>

    <?php if($new): ?>
      <span class="new"><?php print $new ?></span>
    <?php endif; ?>
    
    <?php if ($title): ?>
      <h3><?php print $title ?></h3>
    <?php endif; ?>

    <?php if ($submitted): ?>
      <div class="submitted">
        <?php print $submitted ?>
      </div>
    <?php endif; ?>
    
    <?php if($content): ?>
      <div class="content">
        <?php print $content ?>
      </div>
    <?php endif;?>

    <?php if ($signature): ?>
      <div class="signature">
        <?php print $signature ?>
      </div>
    <?php endif; ?>
    
    <?php if ($links): ?>
      <div class="links">
        <?php print $links ?>
      </div>
    <?php endif; ?>

  </div>
</div>