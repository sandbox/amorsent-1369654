<?php if ($pre_object) print $pre_object ?>

<div <?php if ($attr) print drupal_attributes($attr) ?>>
  <?php if ($edit_links): ?>
    <?php print $edit_links; ?>
  <?php endif; ?>

  <?php if ($title): ?>
    <h2 class = "<?php print $hook; ?>-title"><?php print $title; ?></h2>
  <?php endif;?>

  <?php if ($content): ?>
    <div class="<?php print $hook; ?>-content">
      <?php print $content ?>
    </div>
  <?php endif;?>

</div>

<?php if ($post_object) print $post_object ?>