// Mark body with 'mobile' class when document is under 480 px wide
Drupal.behaviors.seed_mobile = function (context) {
  if($(document).width() < 480) $('body').addClass('mobile');
  
  $(window).resize(function() {
    if($(document).width() < 480) $('body').addClass('mobile');
    else $('body').removeClass('mobile');
  });
}

// Growl-style system messages
Drupal.behaviors.seed_growl = function (context) {
  $('#growl > .messages:not(.seed-processed)').each(function() {
    $(this).addClass('seed-processed');
    // If a message meets these criteria, we don't autoclose
    // - contains a link
    // - is an error or warning
    // - contains a lenghthy amount of text
    if ($('a', this).size() || $(this).is('.error') || $(this).is('.warning') || $(this).text().length > 100) {
      $(this).prepend("<span class='close'>X</span>");
      $('span.close', this).click(function() {
        $(this).parent().hide('fast');
      });
    }
    else {
      // This essentially adds a 5 second pause before hiding the message.
      $(this).animate({opacity:1}, 5000, 'linear').fadeOut(1000);
    }
  });
}

// Collapsible fieldsets 
Drupal.behaviors.seed_fieldsets = function (context) {
  $('.fieldset:not(.seed-processed)').each(function() {
    $(this).addClass('seed-processed');
    
    if ($(this).is('.collapsible')) {
      // If there's an error, don't allow it to be collapsed
      if ($('.error', this).size() > 0) {
        $(this).removeClass('collapsed');
      }
      
      // If it's collapsed, make it so.
      if ($(this).is('.collapsed')) {
        $(this).children('.fieldset-title').siblings('.fieldset-content').hide();
      }
      
      // Add the click handler 
      $(this).children('.fieldset-title').click(function() {
        $(this).siblings('.fieldset-content').slideToggle(20);
        $(this).parent().toggleClass('collapsed');
      });
    }
  });
}